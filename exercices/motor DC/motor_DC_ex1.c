/*
1) Projetar um circuito que tenha 4 teclas com as seguintes funções: a tecla 1 ligará ou
desligará um motor DC, a tecla 2 dará a direção ao motor para frente ou para atrás,
a tecla 3 aumentará a velocidade do motor e a tecla 4 diminuirá a velocidade do
motor. Usar o CI L293D. O microcontrolador será o dsPIC30F4011 com xtal de 8 MHz
e trabalhando com um PLL de 8. A frequência do sinal que controla a velocidade é
de 500Hz.
*/