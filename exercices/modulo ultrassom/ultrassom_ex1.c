// LCD module connections
sbit LCD_RS at LATE0_bit;
sbit LCD_EN at LATE1_bit;
sbit LCD_D4 at LATE2_bit;
sbit LCD_D5 at LATE3_bit;
sbit LCD_D6 at LATE4_bit;
sbit LCD_D7 at LATE5_bit;

sbit LCD_RS_Direction at TRISE0_bit;
sbit LCD_EN_Direction at TRISE1_bit;
sbit LCD_D4_Direction at TRISE2_bit;
sbit LCD_D5_Direction at TRISE3_bit;
sbit LCD_D6_Direction at TRISE4_bit;
sbit LCD_D7_Direction at TRISE5_bit;

float distance, time;
char timeStr[14], distanceStr[14];

/**
 * [Timer1Int description]
 */
void Timer1Int() iv IVT_ADDR_T1INTERRUPT ics ICS_AUTO
{
	IFS0bits.T1IF = 0; /*  */
	time = TMR1*62.5e-9*256; /*  */
	distance = (time*340/2)*100; /*  */
	TMR1 = 0x0000; /*  */
}

/**
 * [main  description]
 */
void main (void)
{

    Lcd_Init(); /* Initialize LCD */
    Lcd_Cmd(_LCD_CLEAR); /* Clear display */
    Lcd_Cmd(_LCD_CURSOR_OFF); /* Cursor off */

    TRISCbits.TRISC14 = 1; /* Echo */
    TRISFbits.TRISF6 = 0; /* Trigger */

    IFS0 = 0; /* Flag de interrupção do timer1 */
    IEC0bits.T1IE = 1; /*  */
    PR1 = 0xFFFF; /*  */
    T1CON = 0x8070; /*  */


	while(1)
	{
		/*  */
		LATFbits.LATF6 = 0;
		Delay_us(2);
		LATFbits.LATF6 = 1;
		Delay_us(11);
		LATFbits.LATF6 = 0;

		/*  */
		if (distance >= 2 && distance <= 400){
			
			/*  */
			FloatToStr(distance, distanceStr);
			FloatToStr(time, timeStr);
			
			/*  */
			Lcd_Out(1, 1, "Time:");
			Lcd_Out(1,6, timeStr);
			Lcd_Out(2, 1, "Dist:");
			Lcd_Out(2,6, distanceStr);
		
		} else {

			/*  */
			Lcd_Cmd(_LCD_CLEAR);
			Lcd_Out(1, 1, "Out of range");
		}

		Delay_ms(1000);
	}
}
