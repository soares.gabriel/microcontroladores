/*

# Experi�ncia 4

Utilizando duas teclas (uma para escolher o valor de PWM e outra para confirmar) mudar a 
velocidade dos motores do carrinho. O valor do PWM dever� aparecer nos displays de 7 
segmentos (a multiplexa��o destes deve ser feito com interrup��es).

O funcionamento do  sistema ser� o seguinte:

* O sistema come�a parado (os displays de 7 segmentos desligados e o carrinho parado);

* Depois de pressionar e soltar (ativar) a tecla de interrup��o INT0:

        * Ficam habilitadas as duas teclas para mudar o valor do PWM;

        * Os valores escolhidos devem ser mostrados no display de 7 segmentos (lembrem que 
          os n�meros devem ser deslocados nos displays quando se pressionam as teclas).

* Depois de confirmar o valor do PWM o carrinho deve ser ativado de acordo com o valor 
  definido (valor que deve ser mostrado no display de 7 segmentos). Quando se confirma 
  um novo valor do PWM a velocidade do carrinho tamb�m mudar�, se o valor do PWM n�o 
  foi confirmado a velocidade do carrinho continua com o valor do PWM anterior.

A frequ�ncia de opera��o do carrinho ser� arbitrariamente determinada. Os valores de 
PWM ser�o de 0 a 100% em passos de 1%.

Pensem em alguma forma de usar o kit de rel�s com as l�mpadas neste exerc�cio, ser� parte 
do funcionamento do circuito. Depois voc�s podem usar o kit de rel�s com as l�mpadas para 
a criatividade se quiserem.

Determinar qual � o valor m�nimo de duty cycle para o qual o motor come�a a girar.

## Criatividade

O controle da velocidade do carrinho ser� alterado arbitrariamente pelos bot�es a qualquer 
momento, o comportamento geral � o seguinte:

        * O carro come�ar� a se deslocar na velocidade m�xima;

        * Se ele n�o encontrar nenhum obst�culo manter� sua velocidade (caso n�o seja 
          alterada pelos bot�es);

        * Se a qualquer momento ele encontrar um obst�culo a uma dist�ncia (arbitrariamente
          determinada):

                * Desvia do obst�culo em alguma dire��o (arbitrariamente determinada);
                * Continua seu percurso �normal�.

O buzzer apitar� de acordo com o sinal de PWM gerado e aplicado nos motores.

*/

#define PWM_OUT_H_BRIDGE_EN LATEbits.LATE5
#define BUZZER LATDbits.LATD3
#define H_BRIDGE_M1_P LATFbits.LATF0
#define H_BRIDGE_M1_N LATFbits.LATF1
#define H_BRIDGE_M2_P LATFbits.LATF4
#define H_BRIDGE_M2_N LATFbits.LATF5
#define DISP0 LATEbits.LATE0
#define DISP1 LATEbits.LATE1
#define DISP2 LATEbits.LATE2
#define ULTRASONIC_TRIGGER LATEbits.LATE3

float distance, time;
unsigned char enablePWM = 0;
int displayCounter = 0;
unsigned int nPWM = 0, dutyCycle, counterDutyCycle,
             centenas, dezenas, unidades, 
             numbers[10] = {0x3F,0x06,0x05B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};

/**
 * Interrup��o externa 2 utilizada para lidar com as intera��es 
 * do bot�o de incremento do duty cycle
 */
void Ext2Int() iv IVT_ADDR_INT2INTERRUPT ics ICS_AUTO
{
        Delay_ms(100);  // Delay para atenuar o efeito do bounce
        
        if(nPWM < 100)  // Enquando o valor do PWM n�o atingir o m�ximo
            nPWM++;     // Incrementa o valor do PWm
        escolher
            nPWM = 0;

        IFS1bits.INT2IF = 0;    // Limpa flag de interrup��o
}

/**
 * Interrup��o externa 0 utilizada para lidar com as intera��es 
 * do bot�o de ativa��o/confirma��o do sistema
 */
void Ext0Int() iv IVT_ADDR_INT0INTERRUPT ics ICS_AUTO
{
    Delay_ms(100); // Delay para atenuar o efeito do bounce

    enablePWM = 1;  // Vari�vel de ativa��o do sistema
    dutyCycle = nPWM;   // Altera��o do duty cycle corrente pelo 
                        // novo valor escolhido
    
    IFS0bits.INT0IF = 0;    // Limpa flag de interrup��o
}

/**
 * Interrup��o externa 2 utilizada para lidar com as intera��es 
 * do bot�o de decremento do duty cycle
 */
void Ext1Int() iv IVT_ADDR_INT1INTERRUPT ics ICS_AUTO
{
    Delay_ms(100);      // Delay para atenuar o efeito do bounce

    if(nPWM > 0)        // Enquanto valor do PWM n�o for negativo
        nPWM--;         // Decermenta o valor do PWM em 1;
    else
        nPWM = 100;     // Se o valor do PWM for negativo, retornar para o valor m�ximo

    IFS1bits.INT1IF = 0;    // Limpa flag de interrup��o
}


/**
 * Interrup��o do Timer 1 utilizada para multiplexar os displays de 
 * 7 segmentos e realizar a separa��o das casas decimais do valor do PWM
 */
void Timer1Int() iv IVT_ADDR_T1INTERRUPT ics ICS_AUTO
{

    /* Desliga os displays de 7 segmentos */
    DISP2 = 0;
    DISP1 = 0;
    DISP0 = 0;

    /* Separa as  casas decimais do n�mero do PWM */
    
    /* Esse m�todo utiliza um vetor com os caracteres dos displays
     * indexados com a posi��o correspondente num�rica. � uma outra
     * forma de obter o mesmo resultado da fun��o mask(), que � 
     * desencorajada dentro da rotina de tratamento da interrup��o.
     */
    
    centenas = nPWM/100;
    dezenas = (nPWM-centenas*100)/10;
    unidades = nPWM-centenas*100-dezenas*10;

    if(enablePWM == 1){           // Checagem da vari�vel de ativa��o
        switch(displayCounter)  // Modifica o comportamento de acordo com o display ativo
        {
            case 0:
                    if(nPWM == 100){                // Caso a centena seja atinjida
                            DISP2 = 1;     // Liga display das centenas
                            LATB = numbers[centenas];   // Exibe valor da centena
                        }
                        else 
                            DISP2 = 0;     // Desliga display das centenas
                        break;

            case 1:
                    if(nPWM >= 10){                 // Caso o valor tenha casa decimal
                            DISP1 = 1;    // Liga o display da casa decimal
                            LATB = numbers[dezenas];    // Exibe o valor ca dasa decimal
                        }
                        else 
                            DISP1 = 0;    // Desliga o display da casa decimal
                        break;

            case 2:
                    DISP0 = 1;        // Display das unidades sempre ligado
                    LATB = numbers[unidades];   // Exibe unidades
                    displayCounter = -1;        // Zera contador dos displays
                    break;
        }
    }

/**
 * Interrup��o do Timer 2 utilizada no modo gate time accumulation para 
 * lidar com o ECHO do sensor ultra s�nico e realizar c�lculos de 
 * dist�ncia necess�rios.
 */
void Timer2Int() iv IVT_ADDR_T2INTERRUPT ics ICS_AUTO
{
        IFS0bits.T2IF = 0; // Zera flag de interrup��o

        time = TMR2*62.5e-9*256;     // C�lculo do tempo: TMR2*Tcy*PRESCALER
        distance = (time*340/2)*100;    // C�lculo da dist�ncia (em cm)
        
        TMR2 = 0x0000;      // Zera TMR2 (modo gated time accumulation)
}


/**
 * Interrup��o do Timer 2 utilizado para gerar a onda do PWM
 */
void Timer3Int() iv IVT_ADDR_T3INTERRUPT ics ICS_AUTO
{
    IFS0bits.T3IF = 0;      // Zera flag de interrup��o

    if(counterDutyCycle > 100 - dutyCycle){ // Duty cycle positivo
        PWM_OUT_H_BRIDGE_EN = 1;        // HIGH
        BUZZER = 1;
    } else {                        // Duty cycle negativo
        PWM_OUT_H_BRIDGE_EN = 0;
        BUZZER = 0;                 // LOW
    }


    if(counterDutyCycle > 99)   // Granularidade do valor de PWM (1%)
        counterDutyCycle = 1;
    else
        counterDutyCycle++;
}

/**
 * Configura��o de rota��o � esquerda
 */
void dirLeft()
{
        /* left motor (black-red): onForward */
        H_BRIDGE_M1_P = 1;
        H_BRIDGE_M1_N = 0;

        /* right motor (yellow-green): onReverse */
        H_BRIDGE_M2_P = 1;
        H_BRIDGE_M2_N = 0;
}

/**
 * Configura��o de rota��o � direita
 */
void dirRight()
{
        /* left motor (black-red): onReverse */
        H_BRIDGE_M1_P = 0;
        H_BRIDGE_M1_N = 1;

        /* right motor (yellow-green): onForward */
        H_BRIDGE_M2_P = 0;
        H_BRIDGE_M2_N = 1;
}
/**
 * Configura��o de movimento avante
 */
/**
 * Configura��o de movimento avante
 */
void dirForward()
{
        /* left motor (black-red): onForward */
        H_BRIDGE_M1_P = 0;
        H_BRIDGE_M1_N = 1;

        /* right motor (yellow-green): onForward */
        H_BRIDGE_M2_P = 1;
        H_BRIDGE_M2_N = 0;

}

/**
 * Configura��o de movimento reverso
 */
void dirReverse()
{
        /* left motor (black-red): onReverse */
        H_BRIDGE_M1_P = 1;
        H_BRIDGE_M1_N = 0;

        /* right motor (yellow-green): onReverse */
        H_BRIDGE_M2_P = 0;
        H_BRIDGE_M2_N = 1;
}

/**
 * Configura��o de repouso
 */
void onHold()
{
        /* left motor (black-red): stop */
        H_BRIDGE_M1_P = 0;
        H_BRIDGE_M1_N = 0;

        /* right motor (yellow-green): stop */
        H_BRIDGE_M2_P = 0;
        H_BRIDGE_M2_N = 0;
}


void main ()
{
    ADPCFG = 0xFFFF; // PORTB como digital
    TRISB = 0;  // PORTB como sa�da (display de 7 segmentos)
    TRISF = 0;  // PORTF como sa�da (morotes ponte H)

    
    /* Configura porta de entrada para sinal ECHO do ultrassom*/
    TRISCbits.TRISC13 = 1;

    /* COnfigura sa�da do  buzzer*/
    TRISDbits.TRISD3 = 0;

    /* Configura sa�da da habilita��o dos displays de 7 segmentos */
    TRISEbits.TRISE0 = 0;
    TRISEbits.TRISE1 = 0;
    TRISEbits.TRISE2 = 0;
    
    /* Configura sa�da do TRIGGER do ultrassom */
    TRISEbits.TRISE3 = 0;
    
    /* Configura sa�da do sinal de EN da ponte H */
    TRISEbits.TRISE5 = 0;

    
    /* Configura bot�es e suas respectivas interrup��es */
    TRISEbits.TRISE8 = 1;
    TRISDbits.TRISD1 = 1;
    TRISDbits.TRISD0 = 1;

    IFS0 = 0; // Limpa flags de interrup��o
    IFS1 = 0; // Limpa flags de interrup��o
    IEC0bits.INT0IE = 1; // Habilitamos INT0
    IEC1bits.INT1IE = 1; // Habilitamos INT1
    IEC1bits.INT2IE = 1; // Habilitamos INT2
    INTCON2bits.INT0EP = 0; //Borda positiva
    INTCON2bits.INT1EP = 0; //Borda positiva
    INTCON2bits.INT2EP = 0; //Borda positiva
    

    /* Configura timer 1 para a multiplexa��o dos displays */
    IEC0bits.T1IE = 1; // Habilita T1
    PR1 = 1000;        // T1 de 62.5 us
    T1CON = 0x8000; // Ativa T1

    /* Configura timer 2 para a utiliza��o do m�dulo'ultrassom */
    IEC0bits.T2IE = 1; // Habilita T2
    PR2 = 0xFFFF;        // T2 de 1 s
    T2CON = 0x8070; // Ativa T2


    /* Configura timer 2 para a gera��o do sinal PWM */
    IEC0bits.T3IE = 1; // Habilita T3
    PR3 = 1600;        // T3 de 100 us
    T3CON = 0x8000; // Ativa T3

    while(1){

            /* Sinal do TRIGGER do m�dulo ultrassom */
            ULTRASONIC_TRIGGER = 0;     // LOW
            Delay_us(2);                // Aguarda 2 us
            ULTRASONIC_TRIGGER = 1;     // HIGH
            Delay_us(11);               // Aguarda 11 us
            ULTRASONIC_TRIGGER = 0;     // LOW

        if(enablePWM == 1){         // Vari�vel de ativa��o

            dirForward();           // Carrinho inicia movimento avante
            
            if (distance >= 2 && distance <= 30){   // Condi��o de desvio
                
                dirReverse(); Delay_ms(500);       // Reverso por 0.5 s

                if (rand() % 2 == 0) {          // Decis�o de dire��o rand�mica 
                    dirLeft(); Delay_ms(500);   // Esquerda por 0.5 s
                } else {                        // ou
                    dirRight(); Delay_ms(500);  // Direita por 0.5 s
                }
                
                dirForward();       // Retoma movimento avante
            }                       
            
            Delay_ms(1000);
        }
    }

}