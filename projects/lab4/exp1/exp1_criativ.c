/*
# Experiência 1

Implementar um programa para um sistema de aquisição de dados com as seguintes
características:

    * Este terá um menu, mostrado no PC, onde se poderá selecionar o tempo de
    aquisição (a cada 0,5 seg, 1 seg, 10 seg, 1 minuto e 1 hora), a aquisição
    será manual para as entradas analógicas 8 e 9.

    * Quando se fizer a conversão em algumas das entradas analógicas, se deve
    fazer 16 conversões na frequência de 8 KHz e tirar a média das tensões
    destas para serem mostradas no PC e no LCD.

A UART deverá ter um baud rate de 19200. Colocar potenciômetros nas entradas
analógicas.

*/


#define N_SAMPLES 16

// conexoes do modulo LCD
sbit LCD_RS at LATE0_bit;
sbit LCD_EN at LATE1_bit;
sbit LCD_D4 at LATE2_bit;
sbit LCD_D5 at LATE3_bit;
sbit LCD_D6 at LATE4_bit;
sbit LCD_D7 at LATE5_bit;
sbit LCD_RS_Direction at TRISE0_bit;
sbit LCD_EN_Direction at TRISE1_bit;
sbit LCD_D4_Direction at TRISE2_bit;
sbit LCD_D5_Direction at TRISE3_bit;
sbit LCD_D6_Direction at TRISE4_bit;
sbit LCD_D7_Direction at TRISE5_bit;
// Fim das conexoes do modulo LCD

float tensao = 0, voltageLevelPin7 = 0, voltageLevelPin8 = 0;
char tensaoSTR[15], //Utilizada para receber a conversão da tensão de float para str.
     message[15],
     updateAllowed = 0;
char m;
unsigned int convertido = 0, size = 0, j = 0;


/**
 * Interrup��o de RX da UART2
 */
void U2RXInterrupt() iv IVT_ADDR_U2RXInterrupt
{
    IFS1bits.U2RXIF = 0;        // limpa flag de interrup��o
    m = U2RXREG;                // insere caractere recebido na viari�vel m
    updateAllowed = 1;          // libera fluxo de compara��o da string recebida
}

/**
 * Fun��o de inicializa��o da UART2
 * @param baudRate taxa de baud rate a ser utilizada
 */
void INIT_UART2(unsigned char baudRate)
{
    U2BRG = baudRate; //Configuramos a UART com 8 bits de dados, 1 bit de parada e sem paridade.
    U2MODE = 0x0000;
    U2STA = 0x0000;
    IPC2 = 0x0440; //Define a faixa de prioridade como média.
    IFS1bits.U2TXIF = 0; //Zera a flag de interrupção de Tx.
    IEC1bits.U2TXIE = 0; //Interrupção de Tx desabilitada.
    IFS1bits.U2RXIF = 0; //Zera a flag de interrupção de Rx.
    IEC1bits.U2RXIE = 1; //Habilita a interrupção de Rx.
    U2MODEbits.USIDL = 1; //Para a operação caso entre em modo ocioso.
    U2MODEbits.UARTEN = 1; //Ativa a UART.
    U2STAbits.UTXEN = 1; //Ativa a transmisão UART.
}

/**
 * Função de envio individual de caractere pela porta serial
 * @param c caractere a ser enviado
 */
void OUTCHR_UART2(unsigned char c)
{
    while (U2STAbits.UTXBF);        // aguarda buffer de TX esvaziar
    U2TXREG = c;                    // insere caractere no registrador de TX
}

/**
 * Função de envio de um vetor de caracteres pela porta serial
 * @param s ponteiro para primeira posição do vetor de caracteres
 */
void OUTSTR_UART2(unsigned char *s)
{
    unsigned int k = 0;                // zera contador
    while(s[k])                        // enquanto não atingir o final da string ('\0')
            OUTCHR_UART2(s[k++]);      // chama função de envio individual de caractere e incrementa contador
}

/**
 * Configura m�dulo ADC para convers�o manual
 */
void INIT_ADC_MANUAL() //masc = mascara para selecionar os pinos que serão analogicos
{
    //ADPCFG = masc; // seleciona pinos de entrada analógicos
    ADCON1 = 0; // controle de sequencia de conversão manual
    ADCSSL = 0; // não é requerida a varredura ou escaneamento
    ADCON2 = 0; // usa MUXA, AVdd e AVss são usados como Vref+/-
    ADCON3 = 0x0007; // Tad = 4 x Tcy = 4* 62,5ns = 250 ns > 153,85 ns (quando Vdd = 4,5 a 5,5V); SAMC = 0 (não é levado em consideraçãoquando é conversão manual)
    ADCON1bits.ADON = 1; // liga o ADC
}

/**
 * Rotina basica de leitura (amostragem manual) do conversor A/D
 * @param  canal canal da entrada anal�gica
 * @return       resultado da convers�o
 */
int READ_ADC_MANUAL(unsigned int canal)
{
    ADCHS = canal; // seleciona canal de entrada analógica
    ADCON1bits.SAMP = 1; // começa amostragem
    Delay_us(125); //tempo de amostragem para 8KHz
    ADCON1bits.SAMP = 0; // começa a conversão
    while (!ADCON1bits.DONE); // espera que complete a conversão
    return ADCBUF0; // le o resultado da conversão.
}

/**
 * Interrup��o do timer 1
 */
void Timer1Int() iv IVT_ADDR_T1INTERRUPT ics ICS_AUTO
{
    IFS0bits.T1IF = 0;
    LATDbits.LATD3 = ~LATDbits.LATD3;
}

/**
 * Interrup��o do timer 3/2
 */
void T3Interrupt() iv IVT_ADDR_T3INTERRUPT ics ICS_AUTO {

     IFS0bits.T3IF = 0;             // limpa flag de interrup��o
     LATBbits.LATB1 = 1;            // ativa LED indicador de convers�o

    for(j = 0; j < N_SAMPLES; j++)  // realiza as 16 amostragens requeridas
    {
        voltageLevelPin7 += READ_ADC_MANUAL(7);        
        voltageLevelPin8 += READ_ADC_MANUAL(8);
    }

    voltageLevelPin7 /= 16;         // realiza a m�dia dos 16 valores lidos
    voltageLevelPin8 /= 16;

    tensao = (voltageLevelPin7*4.0)/1023; // C�lculo da tens�o de entrada
    
    FloatToStr(tensao,tensaoSTR); // Converte a vari�vel float tensao para string, atribuindo o valor na vari�vel tensaoSTR.
    Lcd_Cmd(_LCD_CLEAR); //Limpa o LCD.
    Lcd_Out(1,1,"Vmed:");
    Lcd_Out(1, 9, tensaoSTR); //Imprime a vari�vel tensaoSTR4 no display LCD.

    OUTSTR_UART2(tensaoSTR);    // escreve o valor na porta serial
    OUTSTR_UART2(" V\r\t");     // exibe a unidade de tens�o do valor e tabula sa�da

    tensao = (voltageLevelPin8*4.0)/1023; // C�lculo da tens�o de entrada
    
    FloatToStr(tensao,tensaoSTR); // Converte a vari�vel float tensao para string, atribuindo o valor na vari�vel tensaoSTR.
    Lcd_Out(2,1,"Pot.");
    Lcd_Out(2, 9, tensaoSTR); //Imprime a vari�vel tensaoSTR4 no display LCD.

    OUTSTR_UART2(tensaoSTR);    // escreve o valor na porta serial
    OUTSTR_UART2(" V\r\n");     // exibe a unidade de tens�o do valor 

    /* ativa l�mpada do kit de rel�s caso a luminosidade atinga n�ves baixos */
    if ((READ_ADC_MANUAL(6)*4.0)/1023 > 1.0){
        LATBbits.LATB0 = 1;
    } else {
        LATBbits.LATB0 = 0;
    }

    /* ativa o buzzer de acordo com o n�vel de tens�o do potenci�metro em B8 */
    if (voltageLevelPin8*20 > 30)
    {
        PR1 = 0xFFFF;
        T1CON = 0x8000;
    } else if (voltageLevelPin8*20 > 60)
    {
        PR1 = 31250;
        T1CON = 0x8000;
    } else {
        PR1 = 15625;
        T1CON = 0x8000;
    }

}

int main(void)
{
    ADPCFG = 0xFE3F; // Pino RB8 e RB7 como entrada analógica (0 = analógica, 1=digital)
    TRISB = 0xFFF0; // Porta B como entrada

    TRISDbits.TRISD3 = 0;       // habilita buzzer

    Lcd_Init();// Inicializa LCD
    Lcd_Cmd(_LCD_CLEAR); // Limpa o LCD
    Lcd_Cmd(_LCD_CURSOR_OFF); // Cursor off
    
    IFS0 = 0;               // limpa flags de interrup��o
    IEC0bits.T3IE = 1;      // habilita timer 3/2
    IEC0bits.T3IE = 1;      // habilita timer 1

    INIT_UART2(51);            // Configura UART2 com baud rate de 19200 (Fcy = 8MHz*8 PLL/4)
    INIT_ADC_MANUAL();          // Configura  ADC para opera��o manual

    /* exibe mensagem que apresenta o menu no PC */
    OUTSTR_UART2("Selecione o tempo de aquisi��o:\r\n\n 1: a cada 0,5 seg\r\n 2: a cada 1 seg\r\n 3: a cada 10 seg\r\n 4: a cada 1 minuto\r\n 5: a cada 1 hora\r\n");

    while(1)
    {
        if(updateAllowed == 1)      // caso tenha recebido um caractere permite seguir
        {
            switch(m - '0'){    // escolhe a a��o de acordo com a op��o selecionada
                case 1: OUTSTR_UART2("Tempo de aquisi��o a cada 0,5 seg\r\n");
                        /* coonfigura timer 3/2 para operar a cada 500 ms */
                        PR2 = 31250;
                        PR3 = 0x0000;
                        T2CON = 0x8038;
                        break;

                case 2: OUTSTR_UART2("Tempo de aquisi��o a cada 1 seg\r\n");
                        /* coonfigura timer 3/2 para operar a cada 1 s */
                        PR2 = 62500;
                        PR3 = 0x0000;
                        T2CON = 0x8038;                
                        break;

                case 3: OUTSTR_UART2("Tempo de aquisi��o a cada 10 seg\r\n");
                        /* coonfigura timer 3/2 para operar a cada 10 s */
                        PR2 = 35185;
                        PR3 = 9;
                        T2CON = 0x8038;                
                        break;

                case 4: OUTSTR_UART2("Tempo de aquisi��o a cada 1 min\r\n");
                        /* coonfigura timer 3/2 para operar a cada 1 min */
                        PR2 = 14505;
                        PR3 = 57;
                        T2CON = 0x8038;                
                        break;

                case 5: OUTSTR_UART2("Tempo de aquisi��o a cada 1 hora\r\n");
                        /* coonfigura timer 3/2 para operar a cada 1 h */
                        PR2 = 18345;
                        PR3 = 3433;
                        T2CON = 0x8038;                
                        break;
                                                                                                                        
                default:OUTSTR_UART2("Op��o inv�lida!\r\n");    // caso default
                        OUTSTR_UART2("Selecione o tempo de aquisi��o:\r\n\n 1: a cada 0,5 seg\r\n 2: a cada 1 seg\r\n 3: a cada 10 seg\r\n 4: a cada 1 minuto\r\n 5: a cada 1 hora\r\n");
                        T2CON = 0x0000;     // desativa timers
                        Lcd_Cmd(_LCD_CLEAR); //Limpa o LCD.
            }
        }
        updateAllowed = 0;      // zera flag de atualiza��o
    }
}