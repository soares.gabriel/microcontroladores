/*
Exemplo: Incrementar o valor da porta B a cada décimo pulso. Neste exemplo o timer1 é usado para
contar pulsos de clock externos no pino T1CK. Depois de dez pulsos acontece a interrupção no timer1 e o
valor da porta B é incrementada.
*/

void __attribute__ ((interrupt, no_auto_psv)) _T1Interrupt(void)
{
	IFS0bits.T1IF = 0;
	LATB++; //incrementamos o valor da porta B.
}

//************* Programa Principal *************************
void main (void)
{
	ADPCFG = 0xFFFF;
	//configura a porta B (PORTB) como entradas/saidas digitais
	TRISB=0;
	//a PORTB como saída
	TRISC = 0x4000;
	// PORTC<14>=1; pino T1CKde entrada
	IFS0=0;
	//Flag de interrupção do timer1
	LATB=0;
	IEC0 = IEC0 | 0x0008; // bit 3 do registrador IEC0 habilita a interrupção do timer1 (IEC0bits.T1IE=1)
	PR1 = 10; //O registrador de periodo PR1 é igual a 10
	T1CON=0x8006; //ativamos o timer1 como contador síncrono, o Prescaler fica em 1, ativamos TSYNC e TCS(T1CK)
	while(1); //laço infinito
}