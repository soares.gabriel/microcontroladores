void servoRotate0() //0 Degree
{
  unsigned int i;
  for(i=0;i<50;i++)
  {
    LATBbits.LATB0 = 1;
    Delay_us(800);
    LATBbits.LATB0 = 0;
    Delay_us(19200);
  }
}

void servoRotate90() //90 Degree
{
  unsigned int i;
  for(i=0;i<50;i++)
  {
    LATBbits.LATB0 = 1;
    Delay_us(1500);
    LATBbits.LATB0 = 0;
    Delay_us(18500);
  }
}

void servoRotate180() //180 Degree
{
  unsigned int i;
  for(i=0;i<50;i++)
  {
    LATBbits.LATB0 = 1;
    Delay_us(2200);
    LATBbits.LATB0 = 0;
    Delay_us(17800);
  }
}

void main()
{
  ADPCFG = 0xFFFF;
  TRISB = 0x0000; // PORTB as Ouput Port
  do
  {
    servoRotate0(); //0 Degree
    Delay_ms(1000);
    servoRotate90(); //90 Degree
    Delay_ms(1000);
    servoRotate180(); //180 Degree
  }while(1);
}