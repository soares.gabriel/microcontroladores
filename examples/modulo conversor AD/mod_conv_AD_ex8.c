// conexoes do modulo LCD
sbit LCD_RS at LATE0_bit;
sbit LCD_EN at LATE1_bit;
sbit LCD_D4 at LATE2_bit;
sbit LCD_D5 at LATE3_bit;
sbit LCD_D6 at LATE4_bit;
sbit LCD_D7 at LATE5_bit;
sbit LCD_RS_Direction at TRISE0_bit;
sbit LCD_EN_Direction at TRISE1_bit;
sbit LCD_D4_Direction at TRISE2_bit;
sbit LCD_D5_Direction at TRISE3_bit;
sbit LCD_D6_Direction at TRISE4_bit;
sbit LCD_D7_Direction at TRISE5_bit;
// Fim das conexoes do modulo LCD

float tensao = 0;
char tensaoSTR[15]; //Utilizada para receber a convers�o da tens�o de float para str.
int convertido = 0;

void init_ADC_man() //masc = mascara para selecionar os pinos que ser�o analogicos
{
    //ADPCFG = masc; // seleciona pinos de entrada anal�gicos
    ADCON1 = 0; // controle de sequencia de convers�o manual
    ADCSSL = 0; // n�o � requerida a varredura ou escaneamento
    ADCON2 = 0; // usa MUXA, AVdd e AVss s�o usados como Vref+/-
    ADCON3 = 0x0007; // Tad = 4 x Tcy = 4* 62,5ns = 250 ns > 153,85 ns (quando Vdd = 4,5 a 5,5V); SAMC = 0 (n�o � levado em considera��oquando � convers�o manual)
    ADCON1bits.ADON = 1; // liga o ADC
} //init_ADC_man
//Rotina basica de leitura (amostragem manual) do conversor A/D

int ler_ADC_man(int canal)
{
    ADCHS = canal; // seleciona canal de entrada anal�gica
    ADCON1bits.SAMP = 1; // come�a amostragem
    delay_us(10); //tempo de amostragem
    ADCON1bits.SAMP = 0; // come�a a convers�o
    while (!ADCON1bits.DONE); // espera que complete a convers�o
    return ADCBUF0; // le o resultado da convers�o.
}

// ------------------------ PROGRAMA PRINCIPAL--------------------------------------------//
int main(void)
{
    ADPCFG = 0xFEFF; // Pino RB8 como entrada anal�gica (0 = anal�gica, 1=digital)
    TRISBbits.TRISB8 = 1;
    Lcd_Init();// Inicializa LCD
    Lcd_Cmd(_LCD_CLEAR); // Limpa o LCD
    Lcd_Cmd(_LCD_CURSOR_OFF); // Cursor off
    init_ADC_man();
    while(1)
    {
        convertido = ler_ADC_man(8); //Ler o valor convertido na entrada anal�gica do ADC
        tensao = (convertido*5.0)/1023;
        FloatToStr(tensao,tensaoSTR); // Converte a vari�vel float tensao para string, atribuindo o valor na vari�vel tensaoSTR.
        Lcd_Cmd(_LCD_CLEAR); // Limpa o LCD
        Lcd_Out(1,1,"V: "); // Mostra valor da tens�o no LCD
        Lcd_Out(1,4,tensaoSTR); // Mostra valor da tens�o no LCD
        delay_ms(1000);
    }
}