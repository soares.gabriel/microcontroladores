/*
6. Se quer projetar um sistema com as seguintes características:
	
	a) Quando pressionar e soltar a tecla ligada ao pino onde se encontra a
	entrada da interrupção externa 0 uma primeira vez, os displays (1, 2, 3 e 4)
	deverão mostrar a palavra “HOLA” piscando a intervalos de tempo de 250 ms.
	
	b) Quando pressionar e soltar a tecla ligada ao pino onde se encontra a
	entrada da interrupção externa 0 uma segunda vez, o item a) para onde
	estiver e o display 1 começa a contar de 0 a 9 sem parar a intervalos de 500 ms.
	
	c) Quando pressionar e soltar a tecla ligada ao pino onde se encontra a
	entrada da interrupção externa 0 uma terceira vez, o item b) para onde
	estiver e o display 1 começa a contar de forma decrescente sem parar a
	intervalos de 500 ms.
	
	d) Na quarta vez que pressionar e soltar a tecla ligada ao pino onde se
	encontra a entrada da interrupção externa 0, o item c) para onde
	estiver e começa o item a) e assim sucessivamente. Levar em consideração
	que a interrupção externa 0 se ativa na borda de descida. Fazer o programa
	que execute este enunciado usando as interrupções.
*/

//Solução:
unsigned char cont = 0;
unsigned int i;
unsigned int num0_9[10]={63,6,91,79,102,109,125,7,127,111};
//0,1,2,3,4,5,6,7,8,9
//unsigned int num9_0[10]={111,127,7,125,109,102,79,91,6,63};
//9,8,7,6,5,4,3,2,1,0
//************* ISR da interrupção externa 0 *
void EXT0Int() iv IVT_ADDR_INT0INTERRUPT
{
	cont++;
	IFS0bits.INT0IF = 0;
}
//** Programa Principal ******
void main ()
{
	ADPCFG = 0xFFFF;
	//Configurando pinos de saída para os displays
	TRISC.B13 = 0;
	//Display 1
	TRISC.B14 = 0;
	//Display 2
	TRISD.B2 = 0;
	//Display 3
	TRISD.B0 = 0;
	//Display 4
	//Desligando os displays
	LATC.B13 = 0;
	LATC.B14 = 0;
	LATD.B2 = 0;
	LATD.B0 = 0;
	TRISB=0; //a PORTB como saída
	TRISE = 0x0100; //entrada INT0=RE8
	IFS0 = 0;
	IEC0bits.INT0IE = 1;//Habilita INT0
	INTCON2bits.INT0EP = 0; //Borda positiva ou subida
	while( 1 )
	{
		switch (cont)
		{
			case 0: LATC.B13 = 0;
					LATC.B14 = 0;
					LATD.B2 = 0;
					LATD.B0 = 0;
					break;
			
			case 1: i = 50;
					while(i != 0) //Enquanto não passar 500 mostrando
								  //contagem nos displays de 7 segmentos
					{
						LATD.B0 = 1; //habilita os displays
						LATB=0x76; //letra H
						Delay_ms(2); //atraso de 2 milisegundos
						LATD.B0 = 0; //desabilita display do milhar ms fica
						LATD.B2 = 1; //habilita os displays
						LATB=0x3F; //letra O
						Delay_ms(2); //atraso de 2 milisegundos
						LATD.B2 = 0; //desabilita display das centenas
						LATC.B14 = 1; //habilita os displays
						LATB=0x38; //letra L
						Delay_ms(2); //atraso de 2 milisegundos
						LATC.B14 = 0; //desabilita display das dezenas
						LATC.B13 = 1; //habilita os displays
						LATB=0x77; //letra A
						Delay_ms(2); //atraso de 2 milisegundos
						LATC.B13 = 0; //desabilita display das unidades
						i--;
						if (cont == 2){i=0; break;} //se foi pressionada novamente 
													// a tecla e cont foi para 2
					}
					LATC.B13 = 0;
					LATC.B14 = 0;
					LATD.B2 = 0;
					LATD.B0 = 0;
					if (cont == 2){break;} //se foi pressionada novamente
										   // a tecla e cont foi para 2
					Delay_ms(250);
					i = 0;
					break;
			
			case 2: LATC.B13 = 0;
					LATC.B14 = 0;
					LATD.B2 = 0;
					LATD.B0 = 0;
					LATC.B13 = 1;
					while(1)
					{
						LATB = num0_9[i];
						if (i == 9){i= -1;}
						i++;
						if (cont == 3){break;} // novamente a tecla e cont foi para 3
						Delay_ms(500);
						break;
					}
					break;

			case 3: LATC.B13 = 0;
					LATC.B14 = 0;
					LATD.B2 = 0;
					LATD.B0 = 0;
					LATC.B13 = 1;
					while(1)
					{
						LATB = num0_9[i];
						if (i == 0){i = 10;}
						i--;
						if (cont == 4){break;} //novamente a tecla e cont foi para 4
						Delay_ms(500);
						break;
					}
					break;
			
			default: LATC.B13 = 0;
					LATC.B14 = 0;
					LATD.B2 = 0;
					LATD.B0 = 0;
					cont = 1;
					break;
		}
	}
}