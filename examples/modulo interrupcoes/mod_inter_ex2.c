// 2. Qual é o código para habilitar a interrupção externa 1 e ativar esta na borda de
// descida? E para a interrupção externa 2 que se ativa na borda de subida?
// Responder tudo em um fragmento de código.

void main ()
{
	IFS1 = 0; //aqui se encontra o bit de flag de status da interrupção Externa 1 e 2.
	IEC1bits.INT1IE = 1; //Habilita a interrupção externa 1.
	IEC1bits.INT2IE = 1; //Habilita a interrupção externa 2.
	INTCON2bits.INT1EP = 1; //Se ativa na borda negativa (descida)
	INTCON2bits.INT2EP = 0; //Se ativa na borda positiva (subida)
}