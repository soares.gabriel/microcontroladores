/*
Exemplo 5. Escrever uma fun��o (sa�da de uma string de caracteres) chamada
OUTSTR_UART1 para transmitir caracteres de c�digo ASCII. Utilizar as fun��es
INIT_UART1 e OUTCHR_UART1 para um baud rate de 9600. Seguir configura��o da
UART do item 1.
*/

unsigned char cadena[] ="dsPIC30F4011 em linha;\r\nHola GENTE.\r\n>\0";

//*********** Fun��o INIT_UART1(valor_baud) **********************
void INIT_UART1 (unsigned valor_baud)
{
	U1BRG = valor_baud;
	/*Configuramos a UART, 8 bits de dados, 1 bit de parada,
	sem paridade */
	U1MODE = 0x0000; //Ver tabela para saber as outras configura��es
	U1STA = 0x0000;
	IPC2 = 0x0440; //A faixa de prioridade m�dia, n�o � urgente.
	IFS0bits.U1TXIF = 0; //Zerar o flag de interrup��o de Tx.
	IEC0bits.U1TXIE = 0; //Habilita interrup��o de Tx.
	IFS0bits.U1RXIF = 0; //Zerar o flag de interrup��o de Rx.
	IEC0bits.U1RXIE = 0; //Habilita interrup��o de Rx.
	U1MODEbits.UARTEN = 1; //E liga a UART
	U1STAbits.UTXEN = 1;
}

//************** Fun��o OUTCHR_UART1(char c) ********************
void OUTCHR_UART1(unsigned char c)
{
	while ( U1STAbits.UTXBF); // espera enquanto o buffer de Tx est� cheio.
	U1TXREG = c; // escreve caractere.
}

//************* Fun��o OUTSTR_UART1(char *s) **********************
void OUTSTR_UART1(unsigned char *s)
{
	unsigned int i=0;
	while(s[i]) // la�o at� *s == �\0�, fim da string
	OUTCHR_UART1(s[i++]); // envia o caractere e pronto para o pr�ximo
}

/************* Programa Principal *************************/
int main (void)
{
	INIT_UART1(103); //Inicializa com 9600 bps
	OUTSTR_UART1(cadena); //Escrevendo a string.
	while (1);
}