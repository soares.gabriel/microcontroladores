/*Exerc�cio 3. Escrever uma fun��o (entrada de um caractere) chamada
 INCHR_UART1 para receber um c�digo ASCII de um caractere. Utilizar a
 fun��o INIT_UART para um baud rate de 9600. Mostrar nos leds ligados
 � porta B os c�digos ASCII das teclas digitadas no PC. Seguir
 configura��o da UART do item 1.*/

unsigned char y;

//*********** Fun��o INIT_UART1(valor_baud) **********************
/*Funciona com uma freq��ncia de 8 MHz, e ativando as interrup��es
de Tx e Rx
valor_baud = INT[( (FCY/Desired Baud Rate)/16) � 1]
onde INT = inteiro. */
void INIT_UART1 (unsigned char valor_baud)
{
	U1BRG = valor_baud;
	/*Configuramos a UART, 8 bits de dados, 1 bit de parada, sem paridade */
	U1MODE = 0x0000; //Ver tabela para saber as outras configura��es
	U1STA = 0x0000;
	IPC2 = 0x0440; //A faixa de prioridade m�dia, n�o � urgente.
	IFS0bits.U1TXIF = 0; //Zerar o flag de interrup��o de Tx.
	IEC0bits.U1TXIE = 0; //Habilita interrup��o de Tx.
	IFS0bits.U1RXIF = 0; //Zerar o flag de interrup��o de Rx.
	IEC0bits.U1RXIE = 0; //Habilita interrup��o de Rx.
	U1MODEbits.USIDL = 1;
	U1MODEbits.UARTEN = 1; //E liga a UART
	U1STAbits.UTXEN = 1;
}

//************** Fun��o INCHR_UART1(char c) ********************
unsigned char INCHR_UART1()
{
	unsigned char c;
	while (!U1STAbits.URXDA); // espera enquanto o buffer de Rx est� VAZIO.
	c = U1RXREG ; // recebe caractere.
	return c;
}


/************* Programa Principal *************************/
int main (void)
{
	ADPCFG = 0xFFFF; //configura a porta B (PORTB) como entradas/saidas digitais
	TRISB=0; //a PORTB como sa�da
	INIT_UART1(103); //Inicializa com 9600 bps
	while (1)
	{
		y = INCHR_UART1();
		LATB = y;
	}
}